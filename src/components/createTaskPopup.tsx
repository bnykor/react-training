import React, { useState } from 'react'
import './createTaskPopup.css'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Tasks from '../tasks.json';

let tasks = Tasks


type JSONtasks = {
  id: number;
  title: string;
  description: string;
  estimatedTime: string;
  status: string;
  priority: number;
}


function createTask(id:number, title:string, description:string, estimatedTime:string, status:string, priority:number) {
  return {id, title, description, estimatedTime, status, priority};
}

function CreateTaskPopup({children, setTasks, tasking, trigger, setTrigger}: any): any {
  const [title, setTitle] = useState <string>('')
  const [description, setDescription] = useState <string>('')
  const [estimatedTime, setEstimatedTime] = useState <string>('')
  const [status, setStatus] = useState <string>('')
  const [priority, setPriority] = useState <number>(0)
  const [id, setId] = useState <number>(11)


  const addTask = () => {
    const newTask:JSONtasks = createTask(id,
      title,
      description,
      estimatedTime,
      status,
      priority)
      setId(id+1)
    setTasks(tasking.concat([newTask]))
  };
 
  const handleCencel = () => {
        setTrigger(false);
      };

      const handleAdd = () => {
        addTask();
        setTrigger(false);
      };

  return (trigger) ? (
    <div className='create-popup'>
        <div className="popup-inner">
        <Dialog open={trigger} onClose={handleCencel}>
        <DialogTitle>Create Task</DialogTitle>
        <DialogContent>
          <DialogContentText>
           Please enter Task details...
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Tilte"
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) =>setTitle(e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Description"
            type="email"
            fullWidth
            variant="standard"
            onChange={(e) => setDescription(e.target.value)}
          />
          <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Estimated Time"
          type="date"
          fullWidth
          variant="standard"
          onChange={(e) =>setEstimatedTime(e.target.value)}
        />
        <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Status"
        type="text"
        fullWidth
        variant="standard"
        onChange={(e) => setStatus(e.target.value)}
      />
       <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Priority"
        type="number"
        fullWidth
        variant="standard"
        value={priority}
        onChange={(e) => setPriority(parseInt(e.target.value))}
      />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCencel}>Cancel</Button>
          <Button onClick={handleAdd}>Add</Button>
        </DialogActions>
      </Dialog>
        </div>
        {children}
    </div>
  ): "";
}

export default CreateTaskPopup