import React, { useState } from 'react'
import './createEventPopup.css'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';



function CreateEventPopup(props: any): any {
  

    const handleClickOpen = () => {
        props.setTrigger(true);
      };
    
      const handleClose = () => {
        props.setTrigger(false);
      };

  return (props.trigger) ? (
    <div className='create-popup'>
        <div className="popup-inner">
        <Dialog open={props.trigger} onClose={handleClose}>
        <DialogTitle>Create Event</DialogTitle>
        <DialogContent>
          <DialogContentText>
           Please enter Event details...
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Tilte"
            type="text"
            fullWidth
            variant="standard"
            value={props.title}
            onChange={(e) => props.setTitle(e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Description"
            type="email"
            fullWidth
            variant="standard"
            value={props.description}
            onChange={(e) => props.setDescription(e.target.value)}
          />
          <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Estimated Time"
          type="date"
          fullWidth
          variant="standard"
          value={props.estimatedTime}
          onChange={(e) => props.setEstimatedTime(e.target.value)}
        />
        <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Status"
        type="text"
        fullWidth
        variant="standard"
        value={props.status}
        onChange={(e) => props.setStatus(e.target.value)}
      />
       <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Priority"
        type="number"
        fullWidth
        variant="standard"
        value={props.priority}
        onChange={(e) => props.setPriority(e.target.value)}
      />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleClose}>Subscribe</Button>
        </DialogActions>
      </Dialog>
            {props.children}
        </div>
    </div>
  ) : "";
}

export default CreateEventPopup