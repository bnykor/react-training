import React, { useState } from 'react';
import './createTaskPopup.css'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import { styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Tasks from '../tasks.json';
import Events from '../events.json';
import CreateTaskPopup from './createTaskPopup';
import CreateEventPopup from './createEventPopup';

let tasks = Tasks

type JSONtasks = {
  id: number;
  title: string;
  description: string;
  estimatedTime: string;
  status: string;
  priority: number;
}


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const getRowsData = function(tasking: JSONtasks[]){
  console.log(tasking)
 return <TableBody>
 {tasking.map((row) => (
   <StyledTableRow key={row.id}>
     <StyledTableCell component="th" scope="row">
       {row.id}
     </StyledTableCell>
     <StyledTableCell align="right">{row.description}</StyledTableCell>
     <StyledTableCell align="right">{row.estimatedTime}</StyledTableCell>
     <StyledTableCell align="right">{row.priority}</StyledTableCell>
     <StyledTableCell align="right">{row.status}</StyledTableCell>
     <StyledTableCell align="right">{row.title}</StyledTableCell>

   </StyledTableRow>
 ))}
</TableBody>
}

const getHeader = function(){
  return <TableRow>
  <StyledTableCell>ID</StyledTableCell>
  <StyledTableCell align="right">Title</StyledTableCell>
  <StyledTableCell align="right">Description</StyledTableCell>
  <StyledTableCell align="right">EstimatedTime</StyledTableCell>
  <StyledTableCell align="right">Status</StyledTableCell>
  <StyledTableCell align="right">Other</StyledTableCell>
 </TableRow>
 } 

function TaskHandler({children, setTasks, tasking}: any): any {
   const [createTaskPopupBtn, setcreateTaskPopupBtn] = useState <boolean>(false)
 

  return <div className='create-popup'>
     <Grid container marginTop={'2%'} spacing={1} justifyContent="center">
       <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
        {getHeader()}
        </TableHead>
        {getRowsData(tasking)}
      </Table>
    </TableContainer>
    </Grid>
    <Grid item xs><Button variant="contained" onClick={() => setcreateTaskPopupBtn(true)}>Create</Button></Grid>
    {createTaskPopupBtn && <CreateTaskPopup tasking={tasking} setTasks={setTasks} trigger={createTaskPopupBtn} setTrigger={setcreateTaskPopupBtn}/>}
      
          </div>
}

export default TaskHandler