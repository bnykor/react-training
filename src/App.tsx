import React, { useState, useEffect } from 'react';
import './App.css';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Tasks from './tasks.json';
import Events from './events.json';
import TaskHandler from './components/tasksHandler';


type JSONtasks = {
  id: number;
  title: string;
  description: string;
  estimatedTime: string;
  status: string;
  priority: number;
}

type JSONEvents = {
  id: number;
  title: string;
  description: string;
  estimatedTime: string;
  status: string;
  priority: number;
}

let events:JSONEvents[] = Events
let tasks:JSONtasks[] = Tasks


function App() {
  var [tasking, setTasks] = useState <JSONtasks[]>(tasks)
  var [eventing, setEvents] = useState <JSONtasks[]>(events)
  var [toShow, settoShow] = useState <JSONtasks[]>([])
  const [stat, setStat] = useState <boolean>(true)
  
  const changeStateToEvent = (eventing: JSONtasks[]) =>{
      setStat(false) 
    }
    
  const changeStateToTask = function(tasking: JSONtasks[]){
      setStat(true) 
     }
  
  const changeStateToAll = function(tasking: JSONtasks[]){
      setStat(true) 
     }

  // useEffect(() => {
  //   if (stat) {
  //     settoShow = setTasks;
  //   }
  //   else {
  //     settoShow = setEvents;
  //   }
  // });
  // const [toSend, setToSend] = useState <React.Dispatch<React.SetStateAction<JSONtasks[]>>>(setTasks)
//   const [stat, setStat] = useState <boolean>(true)

// const changeStateToEvent = function(tasking: JSONtasks[]){
//   changeTasks(tasking)
//   setStat(false) 
// }

// const changeStateToTask = function(tasking: JSONtasks[]){
//   changeTasks(tasking)
//   setStat(true) 
//  }
   
// const changeTasks = function(tasking: JSONtasks[]){
//   settoShow(tasking)
//   if (stat) {
//     setToSend(setTasks)
//   }
//   else {
//     setToSend(setEvents)
//   }
//  }

   return (
    <div className="App">
       <Grid container marginTop={'5%'} spacing={1} justifyContent="center">
          <Grid item xs><Button variant="contained">Only tasks</Button></Grid>
          <Grid item xs><Button variant="contained">Only events</Button></Grid>
          <Grid item xs><Button variant="contained">Uncompleted tasks</Button></Grid>
          <Grid item xs><Button variant="contained">High priority tasks</Button></Grid>
       </Grid>
       <TaskHandler tasking={stat ? tasking : eventing} setTasks={stat ? setTasks:setEvents}></TaskHandler>
       <Grid container marginTop={'5%'} spacing={1} justifyContent="center">
          <Grid item xs><Button variant="contained" onClick={() => changeStateToTask(tasking)}>Tasks list</Button></Grid>
          <Grid item xs><Button variant="contained" onClick={() => changeStateToEvent(eventing)}>Events list</Button></Grid>
          <Grid item xs><Button variant="contained" onClick={() => settoShow(eventing.concat(tasking))}>Today tasks and events</Button></Grid>
       </Grid>

    </div> 
  );
}

export default App;
